
# Sofex Project




## Deployment

To deploy this project run

```bash
  docker-compose up
```
After the docker containers are deployed go to the terminal in the initial folder of the app and execute the following commands

```bash
  npm run migrate
  npm run start
```
Then you will be able to use postman or any app you like to test the environment
## Usage/Examples
You might use the following endpoint to get filtered results for the body filters the filter and comparison fields must be arrays with the following shape and you can add as many filters and comparison as you like taking into account objection rules. 

```url
http://localhost:3000/filteredUser
http://localhost:3000/filteredTask
```
```json
{
  "filter": [{"criteria": "email", "value": "test@gmail.com"}],
  "comparison": [{"column": "active", "comparison": "=", "value": true}]
}
```
The get all and find by id routes are the following

```url
http://localhost:3000/user
http://localhost:3000/user/:id
http://localhost:3000/task
http://localhost:3000/task/:id
```
The update and delete routes are the following 

```url
http://localhost:3000/createUser
http://localhost:3000/updateUser
http://localhost:3000/deleteUser

http://localhost:3000/createTask
http://localhost:3000/updateTask
http://localhost:3000/deleteTask
```

The corresponding body for the request are the following 

```json
(JSON FOR CREATE) when updating add "id: string" and for deletion only add the id to the body

UserInterface 
{
    name: string,
    email: string,
    roleId: string,
}

TaskInterface 
{
    title: string,
    description: string,
    hours: number,
    deadline: Date,
    completed: boolean,
    cost: number,
    usersId: string[]
}
```
## Authors

- [@PedroCadevilla](https://gitlab.com/pedrocadevilla)

