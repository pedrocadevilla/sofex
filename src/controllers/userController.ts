import { Request, Response } from "express";
import { UserService } from "../services/userService";
import { userMapper } from "../helpers/mappers";

const userService = new UserService();
export class UserController {
 async createUser(req: Request, res: Response) {
  try {
   const userInformation = userMapper(req.body);
   const user = await userService.createUser(userInformation);
   res.json(user);
  } catch (err) {
   console.error(err);
   res.status(500).json(err);
  }
 }

 async updateUser(req: Request, res: Response) {
  try {
   const userInformation = userMapper(req.body);
   const user = await userService.updateUser(req.body.id, userInformation);
   res.json(user);
  } catch (err) {
   console.error(err);
   res.status(500).json(err);
  }
 }

 async deleteUser(req: Request, res: Response) {
  try {
   const user = await userService.deleteUser(req.body.id);
   res.json(user);
  } catch (err) {
   console.error(err);
   res.status(500).json(err);
  }
 }
 async getFilteredUsers(req: Request, res: Response) {
  try {
   const user = await userService.findFilteredUsers(req.body.filter, req.body.comparison);
   res.json(user);
  } catch (err) {
   console.error(err);
   res.status(500).json(err);
  }
 }
 async getUsersById(req: Request, res: Response) {
  try {
   const user = await userService.findById(req.body.id);
   res.json(user);
  } catch (err) {
   console.error(err);
   res.status(500).json(err);
  }
 }
 async getUsers(req: Request, res: Response) {
  try {
   const users = await userService.findAll();
   res.json(users);
  } catch (err) {
   console.error(err);
   res.status(500).json(err);
  }
 }
}
