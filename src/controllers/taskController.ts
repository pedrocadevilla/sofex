import { Request, Response } from "express";
import { TaskService } from "../services/taskService";
import { taskMapper } from "../helpers/mappers";
import { UserTaskService } from "../services/userTaskService";
import { TaskInterface } from "interfaces/taskInterface";
import { TypeId } from "../utils/types/TypeId";
import UserTaskModel from "../db/models/userTaskModel";
import { UserTaskInterface } from "../interfaces/userTaskInterface";

const taskService = new TaskService();
const userTaskService = new UserTaskService();
export class TaskController {
 async createTask(req: Request, res: Response) {
  try {
   const taskInformation = taskMapper(req.body);
   const task: TaskInterface = await taskService.createTask(taskInformation);
   if (taskInformation.usersId) {
    taskInformation.usersId.forEach(async (id: TypeId) => {
     await userTaskService.createUserTask({ userId: id, taskId: task.id! });
    });
   }
   res.json(task);
  } catch (err) {
   console.error(err);
   res.status(500).json(err);
  }
 }

 async updateTask(req: Request, res: Response) {
  try {
   const taskInformation = taskMapper(req.body);
   const currentAssignedTask = await UserTaskModel.query().where("taskId", "=", req.body.id);
   if (req.body.usersId) {
    const usersUnassigned = currentAssignedTask.filter((x: UserTaskInterface) => !req.body.usersId.includes(x.userId));
    if (usersUnassigned.length > 0) usersUnassigned.forEach(async (x: UserTaskInterface) => await userTaskService.deleteUserTask(x.id!));
    const usersForAssign = req.body.usersId.filter((x: TypeId) => currentAssignedTask.find((y) => y.userId === x) === undefined);
    if (usersForAssign.length > 0) usersForAssign.forEach(async (x: TypeId) => await userTaskService.createUserTask({ userId: x, taskId: req.body.id }));
   }
   const task = await taskService.updateTask(req.body.id, taskInformation);
   res.json(task);
  } catch (err) {
   console.error(err);
   res.status(500).json(err);
  }
 }

 async deleteTask(req: Request, res: Response) {
  try {
   const task = await taskService.deleteTask(req.body.id);
   res.json(task);
  } catch (err) {
   console.error(err);
   res.status(500).json(err);
  }
 }
 async getFilteredTasks(req: Request, res: Response) {
  try {
   const task = await taskService.findFilteredTasks(req.body.filter, req.body.comparison);
   res.json(task);
  } catch (err) {
   console.error(err);
   res.status(500).json(err);
  }
 }
 async getTasksById(req: Request, res: Response) {
  try {
   const task = await taskService.findById(req.body.id);
   res.json(task);
  } catch (err) {
   console.error(err);
   res.status(500).json(err);
  }
 }
 async getTasks(req: Request, res: Response) {
  try {
   const tasks = await taskService.findAll();
   res.json(tasks);
  } catch (err) {
   console.error(err);
   res.status(500).json(err);
  }
 }
}
