import { Request, Response } from "express";
import { roleMapper } from "../helpers/mappers";
import { RoleService } from "../services/roleService";

const roleService = new RoleService();
export class RoleController {
 async createRole(req: Request, res: Response) {
  try {
   const RoleInformation = roleMapper(req.body);
   const Role = await roleService.createRole(RoleInformation);
   res.json(Role);
  } catch (err) {
   console.error(err);
   res.status(500).json(err);
  }
 }

 async updateRole(req: Request, res: Response) {
  try {
   const RoleInformation = roleMapper(req.body);
   const Role = await roleService.updateRole(req.body.id, RoleInformation);
   res.json(Role);
  } catch (err) {
   console.error(err);
   res.status(500).json(err);
  }
 }

 async deleteRole(req: Request, res: Response) {
  try {
   const Role = await roleService.deleteRole(req.body.id);
   res.json(Role);
  } catch (err) {
   console.error(err);
   res.status(500).json(err);
  }
 }
 async getFilteredRoles(req: Request, res: Response) {
  try {
   const Role = await roleService.findFilteredRoles(req.body.filter, req.body.comparison);
   res.json(Role);
  } catch (err) {
   console.error(err);
   res.status(500).json(err);
  }
 }
 async getRolesById(req: Request, res: Response) {
  try {
   const Role = await roleService.findById(req.body.id);
   res.json(Role);
  } catch (err) {
   console.error(err);
   res.status(500).json(err);
  }
 }
 async getRoles(req: Request, res: Response) {
  try {
   const Roles = await roleService.findAll();
   res.json(Roles);
  } catch (err) {
   console.error(err);
   res.status(500).json(err);
  }
 }
}
