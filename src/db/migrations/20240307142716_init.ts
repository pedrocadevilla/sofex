import { Knex } from "knex";
import { defaultHistoryFields, deleteOnUpdateTrigger, createOnUpdateTrigger } from "../helpers/utils";
import { ON_UPDATE_TIMESTAMP_FUNCTION, DROP_ON_UPDATE_TIMESTAMP_FUNCTION, CREATE_UUID, DROP_UUID } from "../helpers/raw";

export async function up(knex: Knex): Promise<void> {
 await knex.raw(CREATE_UUID);
 await knex.raw(ON_UPDATE_TIMESTAMP_FUNCTION);
 await knex.schema.createTable("role", (table) => {
  table.uuid("id", { primaryKey: true }).defaultTo(knex.raw("uuid_generate_v4()"));
  table.string("name", 100).notNullable();
  defaultHistoryFields(knex, table);
 });
 await knex.schema.createTable("user", (table) => {
  table.uuid("id", { primaryKey: true }).defaultTo(knex.raw("uuid_generate_v4()"));
  table.string("name", 100).notNullable();
  table.string("email", 100).notNullable();
  table.uuid("roleId").notNullable().references("id").inTable("role");
  defaultHistoryFields(knex, table);
 });
 await knex.schema.createTable("task", (table) => {
  table.uuid("id", { primaryKey: true }).defaultTo(knex.raw("uuid_generate_v4()"));
  table.string("title", 100).notNullable();
  table.string("description", 300);
  table.date("deadline").notNullable();
  table.integer("hours").notNullable();
  table.boolean("completed").notNullable().defaultTo(false);
  table.float("cost", 10, 2).notNullable();
  defaultHistoryFields(knex, table);
 });
 await knex.schema.createTable("user_task", (table) => {
  table.uuid("id", { primaryKey: true }).defaultTo(knex.raw("uuid_generate_v4()"));
  table.uuid("userId").notNullable().references("id").inTable("user");
  table.uuid("taskId").notNullable().references("id").inTable("task");
  defaultHistoryFields(knex, table);
 });
 await knex.raw(createOnUpdateTrigger("role"));
 await knex.raw(createOnUpdateTrigger("user"));
 await knex.raw(createOnUpdateTrigger("task"));
 await knex.raw(createOnUpdateTrigger("user_task"));
}

export async function down(knex: Knex): Promise<void> {
 await knex.raw(deleteOnUpdateTrigger("role"));
 await knex.raw(deleteOnUpdateTrigger("user"));
 await knex.raw(deleteOnUpdateTrigger("task"));
 await knex.raw(deleteOnUpdateTrigger("user_task"));

 await knex.schema.dropTable("role");
 await knex.schema.dropTable("user");
 await knex.schema.dropTable("task");
 await knex.schema.dropTable("user_task");

 await knex.raw(DROP_UUID);
 await knex.raw(DROP_ON_UPDATE_TIMESTAMP_FUNCTION);
}
