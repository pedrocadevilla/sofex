import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
 await knex.raw('TRUNCATE TABLE "user_task" CASCADE');
 await knex.raw('TRUNCATE TABLE "task" CASCADE');
 await knex.raw('TRUNCATE TABLE "user" CASCADE');
 await knex.raw('TRUNCATE TABLE "role" CASCADE');
 const roleUUID = crypto.randomUUID();
 await knex("role").insert([
  { id: roleUUID, name: "Admin" },
  { id: crypto.randomUUID(), name: "Member" },
 ]);
 const userUUID = crypto.randomUUID();
 await knex("user").insert([{ id: userUUID, name: "Test", email: "test@gmail.com", roleId: roleUUID }]);
 const taskUUID = crypto.randomUUID();
 await knex("task").insert([
  {
   id: taskUUID,
   title: "Task",
   description: "Description task",
   deadline: new Date(),
   hours: 4,
   cost: 450.52,
  },
 ]);

 await knex("user_task").insert([{ userId: userUUID, taskId: taskUUID }]);
}
