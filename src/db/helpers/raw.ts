export const ON_UPDATE_TIMESTAMP_FUNCTION = `
  CREATE OR REPLACE FUNCTION on_update_timestamp()
  RETURNS trigger AS $$
  BEGIN
    NEW."updatedAt" = now();

    IF (NEW.active = false) THEN
      NEW."updatedAt" = OLD."updatedAt";
      NEW."deletedAt" = now();
    END IF;

    RETURN NEW;
  END;
$$ language 'plpgsql';
`;

export const DROP_ON_UPDATE_TIMESTAMP_FUNCTION = "DROP FUNCTION on_update_timestamp";

export const CREATE_UUID = `create extension if not exists "uuid-ossp"`;
export const DROP_UUID = `drop extension if exists "uuid-ossp"`
