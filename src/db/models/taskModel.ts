import { Model, QueryBuilder } from "objection";
import { UserModel } from "./userModel";
import { TaskInterface } from "interfaces/taskInterface";

interface TaskModel extends TaskInterface {}
class TaskModel extends Model {
 static get tableName() {
  return "task";
 }

 static relationMappings = {
  user: {
   relation: Model.ManyToManyRelation,
   modelClass: UserModel,
   filter: (query: QueryBuilder<UserModel>) => query.select("email"),
   join: {
    from: "task.id",
    through: {
     from: "user_task.taskId",
     to: "user_task.userId",
    },
    to: "user.id",
   },
  },
 };
}

export default TaskModel;
