import { Model } from "objection";
import TaskModel from "./taskModel";
import { RoleModel } from "./roleModel";
import { QueryBuilder } from "objection";
export class UserModel extends Model {
 static get tableName() {
  return "user";
 }

 static relationMappings = {
  taskValue: {
   relation: Model.ManyToManyRelation,
   modelClass: TaskModel,
   filter: (query: QueryBuilder<TaskModel>) =>
    query.count("* as completedTask").sum("cost as taskCost").where("completed", "=", false).groupBy("user_task.userId"),
   join: {
    from: "user.id",
    through: {
     from: "user_task.userId",
     to: "user_task.taskId",
    },
    to: "task.id",
   },
  },
  task: {
   relation: Model.ManyToManyRelation,
   modelClass: TaskModel,
   filter: (query: QueryBuilder<TaskModel>) => query.select("title", "description", "cost", "completed"),
   join: {
    from: "user.id",
    through: {
     from: "user_task.userId",
     to: "user_task.taskId",
    },
    to: "task.id",
   },
  },
  role: {
   relation: Model.BelongsToOneRelation,
   modelClass: RoleModel,
   filter: (query: any) => query.select("name"),
   join: {
    from: "user.roleId",
    to: "role.id",
   },
  },
 };
}
