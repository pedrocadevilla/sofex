import { Model } from "objection";

export class RoleModel extends Model {
 static get tableName() {
  return "role";
 }
}
