import { UserTaskInterface } from "interfaces/userTaskInterface";
import { Model } from "objection";

interface UserTaskModel extends UserTaskInterface {}
class UserTaskModel extends Model {
 static get tableName() {
  return "user_task";
 }
}

export default UserTaskModel;
