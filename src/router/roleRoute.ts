import { RoleController } from "../controllers/roleController";
import { Router } from "express";

const RoleRouter = Router();
const roleController = new RoleController();

RoleRouter.get("/role", roleController.getRoles);
RoleRouter.get("/role/:id", roleController.getRolesById);
RoleRouter.post("/createRole", roleController.createRole);
RoleRouter.delete("/deleteRole/:id", roleController.deleteRole);
RoleRouter.put("/updateRole/:id", roleController.updateRole);

export default RoleRouter;
