
import { TaskController } from "../controllers/taskController";
import { Router } from "express";

const TaskRouter = Router();
const taskController = new TaskController();

TaskRouter.get("/task", taskController.getTasks);
TaskRouter.get("/filteredTask", taskController.getTasks);
TaskRouter.get("/task/:id", taskController.getTasksById);
TaskRouter.post("/createTask", taskController.createTask);
TaskRouter.delete("/deleteTask", taskController.deleteTask);
TaskRouter.put("/updateTask", taskController.updateTask);

export default TaskRouter;
