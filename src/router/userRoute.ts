import { Router } from "express";
import { UserController } from "../controllers/userController";
const UserRoute = Router();
const userController = new UserController();

UserRoute.get("/user", userController.getUsers);
UserRoute.get("/filteredUser", userController.getFilteredUsers);
UserRoute.get("/user/:id", userController.getUsersById);
UserRoute.post("/createUser", userController.createUser);
UserRoute.delete("/deleteUser", userController.deleteUser);
UserRoute.put("/updateUser", userController.updateUser);

export default UserRoute;
