import { TypeId } from "utils/types/TypeId";
import { ComparisonFilter, QueryFilter } from "interfaces/queryFilter";
import TaskModel from "../db/models/taskModel";
import { TaskInterface } from "../interfaces/taskInterface";
import UserTaskModel from "../db/models/userTaskModel";
export class TaskService {
 findAll() {
  return TaskModel.query()
   .select("title", "description", "deadline", "hours", "cost", "name as userName", "email as userEmail")
   .join("user_task as ut", "task.id", "ut.taskId")
   .join("user as u", "ut.userId", "u.id");
 }

 findById(id: TypeId) {
  return TaskModel.query()
   .findById(id)
   .select("title", "description", "deadline", "hours", "cost", "name as userName", "email as userEmail")
   .join("user_task as ut", "task.id", "ut.taskId")
   .join("user as u", "ut.userId", "u.id");
 }
 findFilteredTasks(filters: QueryFilter[], comparisons: ComparisonFilter[]) {
  return TaskModel.query()
   .select("title", "description", "deadline", "hours", "cost", "name as userName", "email as userEmail")
   .join("user_task as ut", "task.id", "ut.taskId")
   .join("user as u", "ut.userId", "u.id")
   .modify((queryBuilder) => {
    if (filters.length > 0) {
     filters.forEach(({ criteria, value }) => {
      queryBuilder.where(criteria, value);
     });
    }

    if (comparisons.length > 0) {
     comparisons.forEach(({ column, comparison, value }) => {
      queryBuilder.where(column, comparison, value);
     });
    }
   });
 }
 createTask(Task: TaskInterface) {
  return TaskModel.query().insert(Task);
 }

 async updateTask(id: TypeId, task: TaskInterface) {
  delete task.usersId;
  return TaskModel.query().findById(id).patch(task);
 }

 async deleteTask(id: TypeId) {
  await UserTaskModel.query().delete().where("taskId", "=", id);
  return TaskModel.query().deleteById(id);
 }
}
