import { TypeId } from "utils/types/TypeId";
import { UserModel } from "../db/models/userModel";
import { UserInterface } from "interfaces/userInterface";
import { ComparisonFilter, QueryFilter } from "interfaces/queryFilter";
import UserTaskModel from "../db/models/userTaskModel";

export class UserService {
 findAll() {
  return UserModel.query().withGraphFetched("role").withGraphFetched("taskValue");
 }

 findById(id: TypeId) {
  return UserModel.query().findById(id).withGraphFetched("role").withGraphFetched("taskValue");
 }
 findFilteredUsers(filters: QueryFilter[], comparisons: ComparisonFilter[]) {
  return UserModel.query()
   .withGraphFetched("role")
   .withGraphFetched("taskValue")
   .modify((queryBuilder) => {
    if (filters.length > 0) {
     filters.forEach(({ criteria, value }) => {
      queryBuilder.where(criteria, value);
     });
    }

    if (comparisons.length > 0) {
     comparisons.forEach(({ column, comparison, value }) => {
      queryBuilder.where(column, comparison, value);
     });
    }
   });
 }
 createUser(user: UserInterface) {
  return UserModel.query().insert(user);
 }

 updateUser(id: TypeId, user: UserInterface) {
  return UserModel.query().findById(id).patch(user);
 }

 async deleteUser(id: TypeId) {
  await UserTaskModel.query().delete().where("userId", "=", id);
  return UserModel.query().deleteById(id);
 }
}
