import { TypeId } from "../utils/types/TypeId";
import UserTaskModel from "../db/models/userTaskModel";
import { UserTaskInterface } from "../interfaces/userTaskInterface";

export class UserTaskService {
 createUserTask(userTask: UserTaskInterface) {
  return UserTaskModel.query().insert(userTask);
 }

 async deleteUserTask(id: TypeId) {
  return UserTaskModel.query().deleteById(id);
 }
}
