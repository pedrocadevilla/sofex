import { TypeId } from "utils/types/TypeId";
import { ComparisonFilter, QueryFilter } from "interfaces/queryFilter";
import { RoleModel } from "../db/models/roleModel";
import { RoleInterface } from "../interfaces/roleInterface";

export class RoleService {
 findAll() {
  return RoleModel.query();
 }

 findById(id: TypeId) {
  return RoleModel.query().findById(id);
 }
 findFilteredRoles(filters: QueryFilter[], comparisons: ComparisonFilter[]) {
  return RoleModel.query().modify((queryBuilder) => {
   if (filters.length > 0) {
    filters.forEach(({ criteria, value }) => {
     queryBuilder.where(criteria, value);
    });
   }

   if (comparisons.length > 0) {
    comparisons.forEach(({ column, comparison, value }) => {
     queryBuilder.where(column, comparison, value);
    });
   }
  });
 }
 createRole(Role: RoleInterface) {
  return RoleModel.query().insert(Role);
 }

 updateRole(id: TypeId, Role: RoleInterface) {
  return RoleModel.query().findById(id).patch(Role);
 }

 deleteRole(id: TypeId) {
  return RoleModel.query().deleteById(id);
 }
}
