import { Client } from "pg";
import * as dotenv from "dotenv";
dotenv.config({});

const DB_NAME: string = process.env.DB_NAME || "db-name";
const DB_USER: string = process.env.DB_USER || "postgres";
const DB_HOST: string = process.env.DB_HOST || "localhost";
const DB_PASSWORD: string = process.env.DB_PASSWORD || "root";

async function setupDatabase() {
 const isDev = process.env.NODE_ENV === "development";

 if (!isDev) return console.log("in production environment - skipping database creation.");

 const client = new Client({
  host: DB_HOST,
  user: DB_USER,
  password: DB_PASSWORD,
  port: 5432,
  database: DB_NAME
 });

 await client
  .connect()
  .then(async () => {
   console.log("Connected to PostgreSQL database");
   const res = await client.query(`SELECT datname FROM pg_catalog.pg_database WHERE datname = '${DB_NAME}'`);
   if (res.rowCount === 0) {
    console.log(`${DB_NAME} database not found, creating it.`);
    await client.query(`CREATE DATABASE "${DB_NAME}";`);
    console.log(`created database ${DB_NAME}.`);
   } else {
    console.log(`${DB_NAME} database already exists.`);
   }

   await client.end();
  })
  .catch((err) => {
   console.error("Error connecting to PostgreSQL database", err);
  });
}

setupDatabase();
