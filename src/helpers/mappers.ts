import { RoleInterface } from "interfaces/roleInterface";
import { TaskInterface } from "interfaces/taskInterface";
import { UserInterface } from "interfaces/userInterface";

export function userMapper(user: UserInterface): UserInterface {
 const userInformation: UserInterface = {
  name: user.name,
  email: user.email,
  roleId: user.roleId,
 };
 return userInformation;
}
export function roleMapper(role: RoleInterface): RoleInterface {
 const roleInformation: RoleInterface = {
  name: role.name,
 };
 return roleInformation;
}

export function taskMapper(task: TaskInterface): TaskInterface {
 const taskInformation: TaskInterface = {
  title: task.title,
  description: task.description,
  hours: task.hours,
  deadline: task.deadline,
  completed: task.completed,
  cost: task.cost,
  usersId: task.usersId,
 };
 return taskInformation;
}
