import { TypeId } from "utils/types/TypeId";

export interface RoleInterface {
    id?: TypeId,
    name: string,
}