import { TypeId } from "utils/types/TypeId";
import { RoleInterface } from "./roleInterface";
import { TaskInterface } from "./taskInterface";

export interface UserInterface {
    id?: TypeId
    name: string,
    email: string,
    roleId: TypeId,
    tasks?: TaskInterface[]
}