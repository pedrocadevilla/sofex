export interface QueryFilter {
    criteria: string,
    value: string
}

export interface ComparisonFilter {
    column: string,
    comparison: string,
    value: string,
}