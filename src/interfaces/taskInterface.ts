import { TypeId } from "utils/types/TypeId";

export interface TaskInterface {
    id?: TypeId,
    title: string,
    description: string,
    hours: number,
    deadline: Date,
    completed: boolean,
    cost: number,
    usersId?: TypeId[]
}