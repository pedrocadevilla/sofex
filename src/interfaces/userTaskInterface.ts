import { TypeId } from "utils/types/TypeId";

export interface UserTaskInterface {
    id?: TypeId
    userId: TypeId,
    taskId: TypeId
}