import express, { Application, Request, Response } from "express";
import dotenv from "dotenv";
import { setupDb } from "./db/dbSetup";
import UserRoute from "./router/userRoute";
import RoleRouter from "./router/roleRoute";
import TaskRouter from "./router/taskroute";
import bodyParser from 'body-parser';

dotenv.config();
const app: Application = express();
const port: number = parseInt(process.env.PORT!);
setupDb();
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  }),
);

app.use(UserRoute);
app.use(RoleRouter);
app.use(TaskRouter);
app
 .listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);
 })
 .on("error", (error) => {
  // gracefully handle error
  throw new Error(error.message);
 });
